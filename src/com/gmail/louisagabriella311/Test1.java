package com.gmail.louisagabriella311;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Aug 25, 2021 16:17
 */
public class Test1 {
    public static void main(String[] args) {
        /* CASE
            We have array of integers called nums, write a function to return all numbers (in a form
            of array of integers) that when subtracted by any of integers in nums doesn't return
            number that is < 0
            for example : nums = [3,1,4,2], your output should be : [4], because when 4 is subtracted
            by 3 or 1 or 4 or 2 doesn't return number that is < 0 */

        /* ANSWER
            menurut saya, penyelesaiannya adalah mencari nilai tertinggi di dalam nums,
            karena nilai tertinggi dalam nums tidak akan menghasilkan nilai < 0 saat dikurangi dengan semua nilai lain yang ada di dalam nums array */

        /* scanner */
        Scanner scanner = new Scanner(System.in);

        /*inisialisasi parameter*/
        int size = 0;

        try {
            System.out.print("\n:::INPUT::: *example [1,2,3,4]\nARRAY ");
            String input = scanner.nextLine().replace("[", "").replace("]", "");
            ArrayList<String> inputArr = new ArrayList<>(Arrays.asList(input.split(",")));
            int[] numb = new int[inputArr.size()];
            for (int i = 0; i < inputArr.size(); i++) {
                numb[i] = Integer.parseInt(inputArr.get(i));
            }
//            System.out.print("\n:::INPUT::: \nSIZE ARRAY ");
//            size = scanner.nextInt();
//            int[] nums = new int[size];
//            for (int i = 0; i < size; i++) {
//                System.out.print("ARRAY INDEX "+i+" = ");
//                nums[i] = scanner.nextInt();
//            }
            System.out.print(":::OUTPUT::: " + Arrays.toString(process(numb)) + "\n");
        }
//        catch (InputMismatchException inputMismatchException) {
//            if (size == 0)
//                System.out.print("error ::: SIZE ARRAY is required and must be integer type(number and more than 0)");
//            else
//                System.out.print("error ::: ARRAY INDEX is required and must be integer type(number and more than 0)");
//        }
        catch (NumberFormatException numberFormatException) {
            System.out.print("error ::: ARRAY is required and value must be integer type(number and more than 0) *example [1,2,3,4]");
        } catch (Exception e) {
            System.out.print("error :::");
            e.printStackTrace();
        }
    }

    private static int[] process(int[] nums) {
        int[] result = new int[1];

        for (int i = 0; i < nums.length; i++) {
            if (i == 0)
                result[0] = nums[i];
            else {
                if (nums[i] > result[0])
                    result[0] = nums[i];
            }
        }

        if (result[0] >= 0)
            return result;

        return result;
    }
}
