package com.gmail.louisagabriella311;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Aug 25, 2021 16:17
 */
public class Test2 {
    public static void main(String[] args) {
        /* CASE
            We have array of integers called nums and an integer called x, write a function to return
            all numbers (in a form of array of integers) that when divided by any of integers in nums
            doesn't return x */

        /*input by scanner*/
        Scanner scanner = new Scanner(System.in);
        /*inisialisasi paramter*/
        String input = "";
        Double x = 0.0;

        try {
            System.out.print(":::INPUT::: *example [1,2,3,4]\nnums = ");
            input = scanner.nextLine().replace("[", "").replace("]", "");
            ArrayList<String> numString = new ArrayList<>(Arrays.asList(input.split(",")));
            double[] numb = new double[numString.size()];
            for (int i = 0; i < numString.size(); i++) {
                numb[i] = Double.parseDouble(numString.get(i));
            }
            System.out.print("x = ");
            x = Double.parseDouble(String.valueOf(scanner.nextInt()));

            System.out.print(":::OUTPUT::: " + process(numb, x) + "\n");
        } catch (InputMismatchException inputMismatchException) {
            System.out.print("error ::: x is required and must be integer type");
        } catch (NumberFormatException numberFormatException) {
            System.out.print("error ::: nums is required and value must be integer type(number and more than 0) *example [1,2,3,4]");
        } catch (Exception e) {
            System.out.print("error :::");
            e.printStackTrace();
        }

    }

    private static ArrayList process(double[] nums, double x) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (nums[i] / nums[j] == x) {
                    result.add((int) nums[i]);
                }
            }
        }
        return result;
    }
}
