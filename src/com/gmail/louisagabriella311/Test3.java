package com.gmail.louisagabriella311;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Aug 25, 2021 16:17
 */
public class Test3 {
    public static void main(String[] args) {
        /* CASE
            We have a string called word and an integer called x, write a function to return an array
            of strings containing all strings that has length x.
            for example : word = "souvenir loud four lost", x = 4, your output should be ["loud", "four", "lost"] because those strings has length of 4 */

        /*inisialisasi parameter*/
        String word = "";
        int x = 0;

        /*Scanner input*/
        Scanner scanner = new Scanner(System.in);

        try {
            /*input word*/
            System.out.print("\n:::: INPUT :::: \nword = ");
            word = scanner.nextLine();

            /*input x*/
            System.out.print("x = ");
            x = scanner.nextInt();

            /*validate input*/
            if (word.isEmpty() || x <= 0)
                throw new InputMismatchException();

            /*process*/
            System.out.print("OUTPUT ::: " + satu(word, x));
        } catch (InputMismatchException inputMismatchException) {
            if (word.isEmpty())
                System.out.println("error ::: word is required and must be String type");
            else
                System.out.println("error ::: x is required and must be integer type(number and more than 0)");
        } catch (Exception e) {
            System.out.print("error ::: ");
            e.printStackTrace();
        }
    }

    private static ArrayList<String> satu(String word, int x) {
        String[] wordArray = word.split(" ");
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < wordArray.length; i++) {
            if (wordArray[i].length() == x)
                result.add("\"" + wordArray[i] + "\"");
        }
        return result;
    }
}
